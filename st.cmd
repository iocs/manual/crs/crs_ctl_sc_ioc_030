#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_tcp350
#
require vac_ctrl_tcp350,1.5.1


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_ctrl_tcp350_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: CrS-CTL:Vac-VEPT-893
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_ctrl_tcp350_moxa.iocsh", "DEVICENAME = CrS-CTL:Vac-VEPT-893, IPADDR = crs-ctl-moxa.tn.esss.lu.se, PORT = 4002")

#
# Device: CrS-CTL:Vac-VPT-893
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_pump_tcp350_vpt.iocsh", "DEVICENAME = CrS-CTL:Vac-VPT-893, CONTROLLERNAME = CrS-CTL:Vac-VEPT-893")
