# IOC for CTL Box cryo vacuum turbopumps

## Used modules

*   [vac_ctrl_tcp350](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_tcp350)


## Controlled devices

*   CrS-CTL:Vac-VEPT-893
    *   CrS-CTL:Cryo-TP-893
